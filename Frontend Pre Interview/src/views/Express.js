import React from "react";
import PropTypes from "prop-types";

import Problem from "./Problem.js";

const __onOpen = props => props.handleShow(true);
const Express = props => {
  // 통일되는 style은 묶어서 하는것이 보기도 편할뿐더러 처리하기도편함
  // 하지만 3문제는 아에 고정되있기에
  // 따로빼야함
  return props.show ? (
    <div>
      <Problem
        subject = "3 X 12"
        expectAnswer = {36}
        gainAnswer = {props.source * props.multiply}
      >
      </Problem>
      <Problem
        subject = "3 - (-33)"
        expectAnswer = {36}
        gainAnswer = {props.source - props.substract}
      >
      </Problem>
      <Problem
        subject = "3 + 33"
        expectAnswer = {36}
        gainAnswer = {props.source + props.plus}
      >
      </Problem>
      <p>
        문제) 힝 ㅜㅜ 같은 답이 나와야하는데, 어떻게 고쳐야할까요? (정답 : 36)
      </p>

      <small>
        * 여러분의 동료가 이렇게 코드를 만들었다 가정하고, 코드들 중 어떤 문제가
        있고 해당 코드를 사용했을때 어떤 문제가 우려되는지, 또 고쳐보신 후 왜
        그렇게 고치게 되었는지 주석으로 코드 옆에 남겨주세요. *
      </small>
    </div>
  ) : (
    <button onClick={__onOpen.bind(this,props)}>시험 결과 오픈!</button>
  );
};

// 코드를 보기위한 순서변경
Express.propTypes = {
  source: PropTypes.any.isRequired,

  multiply: PropTypes.any.isRequired,
  plus: PropTypes.number.isRequired,
  substract: PropTypes.number.isRequired,

  show: PropTypes.bool,
  handleShow: PropTypes.func
};

export default Express;
