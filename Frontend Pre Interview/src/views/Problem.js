import React from "react";
import PropTypes from "prop-types";
import correct from "../assets/icon/correct.png";
import incorrect from "../assets/icon/incorrect.png";


let problemCount = 0;

const subjectNumberStyle = {
    fontWeight: "bold",
    display: "inline-block",
    marginRight: "10px"
};
const wappingSubjectStyle = { 
    position: "relative"
};
const answerStyle = {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100px",
    height: "100px"
};

const Problem = props=>(
    <div>
        <p style={wappingSubjectStyle}>
            <span style={subjectNumberStyle}>{++problemCount})</span>
            <span>{props.subject}</span>
            <img
                style={answerStyle}
                src={props.gainAnswer === props.expectAnswer ? correct : incorrect}
                alt="채점"
            />
        </p>
        <p>답 : {props.gainAnswer}</p>
    </div>
);

Problem.propTypes = {
    subject: PropTypes.string.isRequired,
    expectAnswer: PropTypes.number.isRequired,
    gainAnswer: PropTypes.number.isRequired,
  };

export default Problem;