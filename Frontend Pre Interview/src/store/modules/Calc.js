import { createAction, handleActions } from "redux-actions";
import { Map } from "immutable";

// action types
let SHOW_ACTION = "calc/SHOW_ACTION";

// action creator
export let showAction = createAction(SHOW_ACTION);

// initial state
let initialState = Map({
  source: 3,
  show: false
});

// reducer
export default handleActions(
  {
    [SHOW_ACTION]: (state, action) => {
      return state.set("show", true);
    }
  },
  initialState
);
