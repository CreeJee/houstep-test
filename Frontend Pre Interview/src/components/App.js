/* eslint-disable */
import React, { Component } from "react";
import Express from "../views/Express";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as calcActions from "../store/modules/Calc";

// 변하지 않는 코드는 항상 위로 두어야하며 var이 아닌 let 과 같은것으로 두어야함
const mapStateToProps = state => ({
  source: state.Calc.get("source"),
  show: state.Calc.get("show")
});

const mapDispatchToProps = dispatch => ({
  CalcActions: bindActionCreators(calcActions, dispatch)
});

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      multiply: 12,
      substract: -33,
      plus: 33,
    };
  }

  componentDidMount() {
    this.init();
  }


  //불필요한 추상화 제거 
  // 코드가 난잡하면 기본적으로 보기 불편함
  
  // _handleDetecting = () => {
  //   this.setState({
  //     plus: 33
  //   });
  // };

  _handleShow = val => {
    let { CalcActions } = this.props;
    let isShow = !!val;
    if(isShow) {
      CalcActions.showAction(val);
      // this._handleDetecting();
    }
    return isShow;
  };

  //불필요한 추상화 제거 

  // setStating() {
  //   return {
  //     sNumber: 13,
  //     pass: function() {
  //       return this.sNumber;
  //     }
  //   };
  // }

  init() {

    //불필요한 추상화 제거 

    // let substract = this.setStating().pass.call({ sNumber: -33 });
    // this.setState({ substract });
  }

  render() {
    let { source, show } = this.props;
    let { multiply, substract, plus } = this.state;
    return (
      <Express
        source={source}
        multiply={multiply}
        show={show}
        plus={plus}
        substract={substract}
        handleShow={this._handleShow}
      />
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
