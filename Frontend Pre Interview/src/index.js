import React from "react";
import ReactDOM from "react-dom";
// import axios from "axios";
import Root from "./Root";

// 전역변수에 대입하는 행위는 전역을 오염시키는 행위가 되니 금기해야함.
// window.axios = axios;

ReactDOM.render(<Root />, document.getElementById("root"));
