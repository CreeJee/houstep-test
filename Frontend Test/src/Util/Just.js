export async function fetchJSON(path,options){
    return await (await fetch(path,options)).json();
}