import React from 'react';
import PropTypes from "prop-types";
import './index.css'; 
const DateEnum = {
    0 : "일",
    1 : "월",
    2 : "화",
    3 : "수",
    4 : "목",
    5 : "금",
    6 : "토",
}
function DateParagraph(props) {
    let dateObj = props.date;
    let month = dateObj.getMonth()+1;
    let day = dateObj.getDate();
    let date = DateEnum[dateObj.getDay()];
    return (
        <p className="dateParagraph" style={props.style}>오늘 {month}월 {day}일 {date}요일</p>
    );
}
DateParagraph.defaultProps = {
    date : new Date(),
};
DateParagraph.propTypes = {
    date :  PropTypes.instanceOf(Date).isRequired,
    style : PropTypes.object
};
export default DateParagraph;
