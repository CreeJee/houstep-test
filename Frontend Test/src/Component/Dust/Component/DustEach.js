import React from 'react';
import PropTypes from 'prop-types';
import './DustEach.css'; 
const DustEach = (props)=>(
    <div className="block">
      <p>{props.title}</p>
      <div className="each">
        <img src="./images/icon-good.png" alt="좋음?"/>
        <span>{props.value}</span>
      </div>
    </div>
);

DustEach.propTypes = {
  title : PropTypes.string.isRequired,
  value : PropTypes.any,
}
export default DustEach;