  import React,{ useState, useEffect } from 'react';
  import PropTypes from 'prop-types';
  import {fetchJSON} from "../../Util/Just.js"
  import './index.css'; 
  
  import DustEach from "./Component/DustEach.js"

  const useDustData = (API_KEY) => {
    const [data,setData] = useState({});
    useEffect(()=>{
      (async ()=>{
        let dustData = await fetchJSON(`http://openapi.seoul.go.kr:8088/${API_KEY}/JSON/ListAvgOfSeoulAirQualityService/1/5`);
        setData(dustData.ListAvgOfSeoulAirQualityService.row[0]);
      })()
    },[API_KEY]);
    return data;
  };
  function Dust(props) {
    const dustData = useDustData(props.API_KEY);
    console.log(dustData);
    return (
      <div className="Dust">
        <DustEach title="미세먼지" value={dustData.PM25}></DustEach>
        <DustEach title="초미세먼지" value={dustData.PM10}></DustEach>
        <DustEach title="오존" value={dustData.OZONE}></DustEach>
        <DustEach title="이탄화질소" value={dustData.NITROGEN}></DustEach>
      </div>
    );
  }
  Dust.propTypes = {
    API_KEY : PropTypes.string.isRequired,
  };

  export default Dust;
