import React,{ useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {fetchJSON} from "../../Util/Just.js"

import './index.css'; 
// https://openweathermap.org/weather-conditions
const WEATHER_ENUM = {
    1 : "맑은 하늘",
    2 : "구름 조금",
    3 : "구름 보통",
    4 : "구름 많이",
    9 : "비 조금",
    10 : "비",
    11 : "뇌우",
    13 : "눈",
    50 : "안개",
}
const KelvinToMetric= (kelvinValue) => (kelvinValue - 273.15).toFixed(2);
const WEATHER_ICON_ENUM = {
    1 : "./images/icon-sun.png",
    2 : "./images/icon-cloud.png",
    3 : "./images/icon-cloud.png",
    4 : "./images/icon-cloud.png",
    9 : "./images/icon-rain.png",
    10 : "./images/icon-rain.png",
    11 : "./images/icon-rain.png",
    13 : "./images/icon-snow.png",
    50 : "./images/icon-mist.png",
};
const useWeatherData = (API_KEY) => {
    const [data,setData] = useState({});
    useEffect(()=>{
      (async ()=>{
        let response = await fetchJSON(`https://api.openweathermap.org/data/2.5/weather?q=Seoul,kr&appid=${API_KEY}`);
        let weatherOnly = response.weather[0];
        weatherOnly.description = WEATHER_ENUM[parseInt(weatherOnly.icon)];
        
        weatherOnly.src = WEATHER_ICON_ENUM[parseInt(weatherOnly.icon)];
        weatherOnly.temp = KelvinToMetric(response.main.temp);
        setData(weatherOnly);
      })()
    },[API_KEY]);
    return data;
  };
function Weather(props) {
    const weatherData = useWeatherData(props.API_KEY);
    console.log(weatherData);
    return (
        <div className="Weather">
            <h1>{props.areaName}</h1>
            <img src={weatherData.src} alt={weatherData.description}/>
            <h2>{weatherData.temp}°C</h2>
            <h2>{weatherData.description}</h2>
        </div>
    );
}
Weather.defaultProps = {
    areaName : "서울",
};
Weather.propTypes = {
    API_KEY : PropTypes.string.isRequired,
    areaName : PropTypes.string.isRequired,
};

export default Weather;
