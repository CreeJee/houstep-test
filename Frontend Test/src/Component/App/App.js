import React from 'react';
import Weather from '../Weather/index.js'
import Dust from '../Dust/index.js'
import Date from '../DateParagraph/index.js'
import './App.css';

function App() {
  return (
    <div className="App">
      <Weather API_KEY="e5854fd4d80673e6ed3702e42659440a"></Weather>
      <Date></Date>
      <Dust API_KEY="4a4d41415343726537324b5a58536b"></Dust>
      <footer>
        <img src="./images/logo.png" alt="HOUSTEP"/>
      </footer>
    </div>
  );
}

export default App;
