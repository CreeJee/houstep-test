
# HOUSTEP Test

## JS질문
https://www.notion.so/HOUSTEP-Pre-Interview-Test-4f8c097cecc34e0fb0151a434360f597

###1.
```javascript
    let incomes = [ 1000, 2000, 3000, 4000 ];
    let vats = incomes.map(v=>v/11);
    let price = incomes.reduce((accr,v)=>accr+=v);
```
###2.
>toISOString는 JS의 Date에서도 지원하는 기능,instanceof 에 undefined붙어서 오류임으로 코드가 고쳐지는것은 아래와 같이 고쳐져야함
```javascript
    const Moment = require('moment');
    const convertParam = (params) => {
        let result  = {};
        const item = {};
        for (const key in params) {
            item = params[key];
            if (item !== undefined) {
                result[key] = item;
            }
            if (item instanceof Moment || item instanceof Date && typeof item.toISOString === "function") {
                result[key] = item.toISOString();
            }
        }
        return result;
    };
    
    const p = { a: 'test', b: moment('2019-05-07'), c: undefined };
```
###3.
```javascript
class Queue{
    constructor(...contents){
        this.queue = contents;
    }
    pop(){
        return this.queue.splice(0,1)[0];
    }
}
class PekableQueue extends Queue{
    constructor(...contents){
        super(...contents)
    }
    peek(){
        return this.quque[0];
    }
}
```
>어느것이 더 좋다 한다고 봤을때 표현식이라고 하면 class로 하는것이 좋으며 super 키워드,prototype 접근 에 대한 강제적인 제약으로 안전하게 로직을 처리할수 있음

## ReactJS
>Frontend Pre Interview 폴더 참조

## 프론트엔드 개발자 과제 평가
>Frontend Test 폴더참고
>Redux 대신 React hooks를 이용해서 간단히 구성

